import { Component, MeshComponent, Property } from '@wonderlandengine/api';
import { CursorTarget, HowlerAudioSource } from '@wonderlandengine/components';
import { hapticFeedback } from './haptic-feedback.js';

export class ToggleLegendHighlight extends Component {
  static TypeName = 'toggle-legend-highlight';
  static Properties = {
    buttonMeshObject: Property.object(),
    targetObject: Property.object(),
    toggleMaterial: Property.material(),
  };

  static onRegister(engine) {
    engine.registerComponent(HowlerAudioSource);
    engine.registerComponent(CursorTarget);
  }

  start() {
    this.mesh = this.buttonMeshObject.getComponent(MeshComponent);
    this.defaultMaterial = this.mesh.material;
    this.buttonMeshObject.getTranslationLocal(this.returnPos);

    this.target = this.object.getComponent(CursorTarget) || this.object.addComponent(CursorTarget);

    this.soundClick = this.object.addComponent(HowlerAudioSource, {
      src: 'sfx/click.wav',
      spatial: true,
    });
    this.soundUnClick = this.object.addComponent(HowlerAudioSource, {
      src: 'sfx/unclick.wav',
      spatial: true,
    });

    // toggled state
    this.toggled = false;

    // Get target property which to be changed
    this.targetMesh = this.targetObject.getComponent(MeshComponent);
    this.defaultTargetMaterial = this.targetMesh.material;

    // Highlighted material
  }

  onActivate() {
    this.target.onHover.add(this.onHover);
    this.target.onUnhover.add(this.onUnhover);
    this.target.onDown.add(this.onDown);
    this.target.onClick.add(this.onClick);
    this.target.onUp.add(this.onUp);
  }

  onDeactivate() {
    this.target.onHover.remove(this.onHover);
    this.target.onUnhover.remove(this.onUnhover);
    this.target.onDown.remove(this.onDown);
    this.target.onClick.remove(this.onClick);
    this.target.onUp.remove(this.onUp);
  }

  /* Called by 'cursor-target' */
  onHover = (_, cursor) => {
    this.hover = true;
    hapticFeedback(cursor.object, 0.2, 50);

    if (cursor.type === 'finger-cursor') {
      this.onDown(_, cursor);
    }
  };

  /* Called by 'cursor-target' */

  onDown = (_, cursor) => {
    return;
  };

  onClick = (_, cursor) => {
    // toggles material on given target

    this.toggled = !this.toggled;

    if (this.toggled) {
      //   console.log('Toggle');

      //   targetObject changes
      this.targetMesh.material = this.toggleMaterial;

      // button object changes
      this.soundClick.play();
      hapticFeedback(cursor.object, 1.0, 20);
      this.mesh.material = this.toggleMaterial;
    } else {
      // on up implemented here
      //   console.log('Untoggle');

      // targetObject changes

      if (this.targetObject.name == 'hlavni_cast_objektu_kostely_obj.001') {
        this.targetMesh.material = this.defaultTargetMaterial;
      } else {
        this.targetMesh.material = this.defaultMaterial;
      }

      // button object changes
      this.soundUnClick.play();
      hapticFeedback(cursor.object, 0.7, 20);

      this.mesh.material = this.defaultMaterial;
    }
  };

  /* Called by 'cursor-target' */
  onUp = (_, cursor) => {
    return;
  };

  /* Called by 'cursor-target' */
  onUnhover = (_, cursor) => {
    this.hover = false;

    if (cursor.type === 'finger-cursor') {
      this.onUp(_, cursor);
    }

    hapticFeedback(cursor.object, 0.1, 50);
  };
}
