import { Component, MeshComponent, Property, TextComponent } from '@wonderlandengine/api';
import { CursorTarget, HowlerAudioSource } from '@wonderlandengine/components';
import { hapticFeedback } from './haptic-feedback.js';

export class SetActive extends Component {
  static TypeName = 'set-active';
  static Properties = {
    /** Object that has the button's mesh attached */
    targetObject: Property.object(),
  };

  static onRegister(engine) {
    engine.registerComponent(HowlerAudioSource);
    engine.registerComponent(CursorTarget);
  }

  start() {
    this.target = this.object.getComponent(CursorTarget) || this.object.addComponent(CursorTarget);

    this.soundClick = this.object.addComponent(HowlerAudioSource, {
      src: 'sfx/click.wav',
      spatial: true,
    });
    this.soundUnClick = this.object.addComponent(HowlerAudioSource, {
      src: 'sfx/unclick.wav',
      spatial: true,
    });

    this.targetMesh = this.targetObject.getComponent(MeshComponent);
    this.isTargetActive = this.targetMesh.active;
  }

  onActivate() {
    this.target.onHover.add(this.onHover);
    this.target.onUnhover.add(this.onUnhover);
    this.target.onDown.add(this.onDown);
    this.target.onClick.add(this.onClick);
    this.target.onUp.add(this.onUp);
  }

  onDeactivate() {
    this.target.onHover.remove(this.onHover);
    this.target.onUnhover.remove(this.onUnhover);
    this.target.onDown.remove(this.onDown);
    this.target.onClick.remove(this.onClick);
    this.target.onUp.remove(this.onUp);
  }

  /* Called by 'cursor-target' */
  onHover = (_, cursor) => {
    hapticFeedback(cursor.object, 0.2, 50);

    if (cursor.type === 'finger-cursor') {
      this.onDown(_, cursor);
    }
  };

  /* Called by 'cursor-target' */

  onDown = (_, cursor) => {
    return;
  };

  onClick = (_, cursor) => {
    // toggles material on given target

    this.targetMesh.active = !this.isTargetActive;
    this.soundClick.play();
    hapticFeedback(cursor.object, 1.0, 20);
    this.clicked = true;
  };

  /* Called by 'cursor-target' */
  onUp = (_, cursor) => {
    return;
  };

  /* Called by 'cursor-target' */
  onUnhover = (_, cursor) => {
    hapticFeedback(cursor.object, 0.2, 50);
  };
}
