# Brno 3D model VR

<img src="readme_images/title_image.png" alt="" width="500">

Application developed as a part of master's thesis on Virtual reality technologies on the web and its relation to the GIS and spatial data.
This web app was developed as a final part of a larger pipeline dealing with the process of getting traditional geospatial data (vector, raster) from GIS into virtual reality. Web app was developed in the Wonderland engine.

_You can find the page here:_ - [BrnoVR](https://wle-maps-en-shtrdl-56545e8d6afc26501356a23794792c24fc853ba577e1.gitlab.io/)

## References

This app couldn't be possible without extensive help from the Wonderland engine community.
As well as without the already existing code, which made possible different kinds of movements and accessibility of the app on different devices.
Many thanks go to [SignorPipo](https://github.com/signorpipo/wle-pp), [Jonathan Hale](https://github.com/Squareys), [Sorskoot](https://github.com/sorskoot) and other kind people on the Wonderland discord.

### Data

Displayed data is the Brno 3D model, which has been finally published. I have worked on the QA for the model data. Data can be viewed and accessed here:

- [GIS app](https://webmaps.kambrno.cz/webmaps.kambrno.cz/3d-model_en/)
- [Data](https://datahub.brno.cz/maps/dc95041d63e44e129ba0d9258a1dddb4/explore?location=49.196016%2C16.608108%2C12.84)

## Thesis

Thesis in czech: [Text](https://github.com/jendahorak/dp/blob/main/Webova_virtu%C3%A1lni_realita_novy_zpusob_prezentace_geoprostorovych_dat_fin.pdf)

For those who are interested the thesis aim was more on the data processing rather than the final user experience. Here are some visuals that have been created in the process.

![sw_pipeline](readme_images/sw_pipeline.png)
